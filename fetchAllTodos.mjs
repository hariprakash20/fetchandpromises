import fetch from "node-fetch"

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(response => console.log(response))
.catch(err => console.error(err.message));