import fetch from "node-fetch"

const getTodos = () => fetch('https://jsonplaceholder.typicode.com/todos');

const getUsersDetails = (id) => fetch('https://jsonplaceholder.typicode.com/users?id='+id);

getTodos().then(response => response.json())
.then(response => {
    let todos = response;
    return Promise.all([todos, getUsersDetails(response[0].id)])})
.then(responseArray => {
    return Promise.all([responseArray[0],responseArray[1].json()]);
})
.then(response => console.log(response))
.catch(err => console.error(err.message));