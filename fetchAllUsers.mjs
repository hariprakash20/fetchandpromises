import fetch from "node-fetch"

fetch('https://jsonplaceholder.typicode.com/users')
.then(response => response.json())
.then(response => console.log(response))
.catch(err => console.log(err.message))