import fetch from "node-fetch"

const getUsers = () => fetch('https://jsonplaceholder.typicode.com/users');

const getUsersDetails = (id) => fetch('https://jsonplaceholder.typicode.com/users?id='+id);

getUsers().then(response => response.json())
.then(response => Promise.all(response.map(user=>  getUsersDetails(user.id))))
.then(responses => Promise.all(responses.map(response => response.json())))
.then(responses => console.log(responses))
.catch(err => console.log(err.message))