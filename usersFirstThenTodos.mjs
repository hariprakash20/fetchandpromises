import fetch from "node-fetch"

const getUsers = () => fetch('https://jsonplaceholder.typicode.com/users');

const getTodos = () => fetch('https://jsonplaceholder.typicode.com/todos');


getUsers().then(response => response.json())
.then(users =>  Promise.all([users,getTodos()]))
.then(response => Promise.all([response[0],response[1].json()]))
.then (response => console.log(response))
.catch(err => console.error(err.message))